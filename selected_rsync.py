import argparse
from pathlib import Path
import os
import shutil

def copy(src, dst, overwrite=False, verbose=False, dry_run=False):
    if not os.path.isfile(src):
        raise ValueError("src needs to be file-paths!")

    os.makedirs(os.path.dirname(dst), exist_ok=True)

    if verbose:
        print("Attempting copy of \"{}\" to \"{}\"".format(src, dst))

    if overwrite and src != dst:
        shutil.rmtree(dst, ignore_errors=True)

    if not dry_run:
        shutil.copy2(src, dst)

def replace_special_chars(string):
    string = string.translate(str.maketrans({"-": "_",
                                             "ö": "oe",
                                             "ü": "ue",
                                             "ä": "ae",
                                             "ß": "ss",
                                             " ": "_",
                                             "?": "_",
                                             "!": "_"
                                             }))
    string = ''.join((c for c in string if 0 < ord(c) < 127))
    return string

def get_copy_dict(config):
    source_files = [path for path in Path(config.source_dir).rglob('*') if os.path.isfile(path)]

    if config.include_file:
        # Filter source files
        try:
            with open(config.include_file, 'r') as f:
                include = [l.rstrip() for l in f.readlines() if not l.startswith('#')]
        except:
            raise

        source_files = [path for path in source_files if any([os.path.commonpath([i]) == os.path.commonpath([i, \
            os.path.relpath(path, config.source_dir)]) for i in include])]

    relative_source_files = [os.path.relpath(path, config.source_dir) for path in source_files]
    dest_files = [replace_special_chars(path) if config.replace_special_chars else path for path in relative_source_files]
    dest_files = [Path(os.path.join(config.dest_dir, path)) for path in dest_files]
    source_files = [Path(path) for path in source_files]

    copy = {k: v for k, v in zip(source_files, dest_files)}

    return copy

def purge_old(config, copy):
    existing_files_in_dest = [Path(path) for path in Path(config.dest_dir).rglob('*') if os.path.isfile(path)]

    for f in existing_files_in_dest:
        if f not in copy.values():
            if config.verbose:
                print("delete: {}".format(f))
            if not config.dry_run:
                os.remove(f)

    # Delete empty dirs:
    for p in list(Path(config.dest_dir).glob('**/*')):
        if p.is_dir() and len(list(p.iterdir())) == 0:
            try:
                os.removedirs(p)
            except FileNotFoundError:
                pass


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Copy stuff.')
    parser.add_argument('source_dir', type=str,
                        help='path to your music folder (expecting structure:\n  source_dir/ARTIST/ALBUM)')
    parser.add_argument('dest_dir', type=str,
                        help='path to your destination music folder (expecting structure:\n  source_dir/ARTIST/ALBUM)')
    parser.add_argument('--include_file', type=str,
                        dest='include_file',
                        help='path to file with artists/albums to include')
    parser.add_argument('--replace_special_chars', dest='replace_special_chars', action='store_true',
                        help='leave only ascii in destination')
    parser.add_argument('--delete', dest='delete', action='store_true',
            help='WARNING: delete everything in destination that is not in include anymore')
    parser.add_argument('--dry_run', dest='dry_run', action='store_true',
                        help='automatically activates verbose -> all actions are printed instead of executed')
    parser.add_argument('--verbose', dest='verbose', action='store_true',
                        help='comment everything that is happening')
    parser.add_argument('--overwrite', dest='overwrite', action='store_true',
                        help='overwrite existing files')

    config = parser.parse_args()
    if config.dry_run:
        config.verbose = True

    copy_list = get_copy_dict(config)

    if config.delete:
        purge_old(config, copy_list)

    for s, d in copy_list.items():
        copy(s, d, overwrite=config.overwrite, verbose=config.verbose, dry_run=config.dry_run)
